﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WCarUI : MonoBehaviour
{
    public WGameSystem system;
    
    public TextMeshProUGUI speedometer;
    public TextMeshProUGUI timer;
    public TextMeshProUGUI lap;
    public TextMeshProUGUI laps;

    public Transform arrow;
    public Animator uiAnim;

    public AudioSource beep;
    public AudioSource overBoop;

    void Update () {
        speedometer.text = Mathf.Abs (system.theCar.zVel).ToString ("000");
        timer.text = system.timer.ToString ("00");
    }

    public void Checkpoint () {
        Action ("ui_checkpoint");
        beep.Play ();
    }

    public void Lap () {
        lap.text = system.lap.ToString ();
        laps.text = system.laps.ToString ();
    }

    public void Win () {
        Action ("ui_youWin");
    }

    public void GameOver () {
        Action ("ui_gameOver");
        overBoop.Play ();
    }

    public void Action (string action) {
        uiAnim.Play (action, 0, 0f);
    }

    public void StartGame () {
        system.Play ();
        Action ("ui_go");
    }

    public void EndGame () {
        system.EndGame ();
    }
}
