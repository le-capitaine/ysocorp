﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WGameSystem : MonoBehaviour
{
    public TheCar theCar;
	public WCam playerCam;
    public WCarUI ui;

    public WCheckpoint[] checkpoints;

    public float timer = 60f;

    public float timerLimit = 60f;

    [Tooltip ("X = max, Y = min, Z = time to reach min")]
    public Vector3 checkpointTime;

    public int laps;

    public int lap;

    float elapsedTime = 0f;

    bool playing;

    int currentCheckpoint = 0;

    void Start () {
        foreach (WCheckpoint p in checkpoints) p.system = this;
        ui.Lap ();
        ui.Action ("ui_countdown");
    }

    public void Play () {
        playing = true;
        theCar.getsInput = true;
    }

    public void FixedUpdate () {
        if (playing) GameUpdate ();
		CamUpdate ();
    }

    void GameUpdate () {
        if (timer <= 0f) {
            timer = 0f;
            GameOver ();
        }
        else {
            elapsedTime += Time.fixedDeltaTime;
            timer -= Time.fixedDeltaTime;
        }

        
        if (checkpoints.Length > 0) ui.arrow.LookAt (checkpoints [currentCheckpoint].transform, Vector3.up);
    }

	void CamUpdate () {
		theCar.zVel = theCar.transform.InverseTransformVector (theCar.thisRigidbody.velocity).z;
		playerCam.orbit.target.transform.localPosition = Vector3.forward * Mathf.Lerp (0f, theCar.camForward, (theCar.zVel / theCar.speed) * theCar.camForward);
		playerCam.orbit.degrees.x = theCar.camPitch + ((theCar.zVel / theCar.speed) * theCar.camWiden);
	}

    public void Checkpoint () {
        timer += (float) Mathf.FloorToInt (Mathf.Lerp (checkpointTime.x, checkpointTime.y, elapsedTime / checkpointTime.z));
        timer = Mathf.Clamp (timer, 0f, timerLimit);

        checkpoints [currentCheckpoint].gameObject.SetActive (false);
        if (currentCheckpoint == 0) Lap ();
        
        if (playing) {
            currentCheckpoint = (currentCheckpoint+1) % (checkpoints.Length);
            checkpoints [currentCheckpoint].gameObject.SetActive (true);

            ui.Checkpoint ();
        }
    }

    public void Lap () {
        if (lap == laps) Win ();
        else {
            lap++;
            ui.Lap ();
        }
    }

    public void Win () {
        checkpoints [currentCheckpoint].gameObject.SetActive (false);
        playing = false;
        theCar.getsInput = false;
        ui.Win ();
    }

    public void GameOver () {
        checkpoints [currentCheckpoint].gameObject.SetActive (false);
        playing = false;
        theCar.getsInput = false;
        ui.GameOver ();
    }

    public void EndGame () {
		WGame.game.input ["thecar/Move"].Disable ();
        WGame.game.LoadScene ("title");
    }
}
