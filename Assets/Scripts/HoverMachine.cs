﻿using UnityEngine;
using System.Collections;

public class HoverMachine : MonoBehaviour {
	
	public Transform model;
	public Handling handling;
	public Hover hovering;

	WGame game;
	Vector3 inputAxes;
	Rigidbody thisShip;

	// Use this for initialization
	void Awake () {
		game = GameObject.FindWithTag ("GameController").GetComponent<WGame>();
		thisShip = GetComponent<Rigidbody> ();
		Init ();
	}

	void Init () {
	}

	// Update is called once per frame
	void FixedUpdate () {
		inputAxes = new Vector3 (Input.GetAxisRaw ("Move Y"),
		                         Input.GetAxisRaw ("Move X"),
		                         -Input.GetAxisRaw ("Move X"));
		Fly ();
	}

	// Update is called once per frame
	void Fly () {
		Vector3 vel = transform.InverseTransformVector (thisShip.velocity);

		Vector3 steerAxes = new Vector3 (0f,
		                                 inputAxes.y * handling.steeringAngles.x,
		                                 0f);

		steerAxes -= new Vector3 (0f, thisShip.angularVelocity.y, 0f);

		thisShip.AddRelativeTorque (steerAxes, ForceMode.VelocityChange);

		float speed = Mathf.Lerp (handling.speed.x, 0, vel.z / handling.maxSpeed);

		if (Input.GetButton("Jump")) {
			thisShip.AddForce (transform.forward * speed,
			                   ForceMode.VelocityChange);
		}

		float bank = steerAxes.x;

		model.localEulerAngles = new Vector3 (0, 0, bank);

		RaycastHit[] cast = hovering.Cast ();

		Vector3 point = Vector3.zero;
		Vector3 normal = Vector3.zero;
		float divider = cast.Length;
		
		foreach (RaycastHit hit in cast) {
			if (hit.rigidbody != thisShip) {
				point += hit.point;
				normal += hit.normal;
			}
			else divider--;
		}
		point *= (1f / divider);
		normal *= (1f / divider);

		if (cast.Length > 0) {
			
			thisShip.useGravity = false;
		


			float distance = Vector3.Distance (transform.position, point);

			float hover = ((hovering.range * hovering.height) - distance)
						  * (hovering.strength - Time.deltaTime);
			hover *= Mathf.Lerp (1, 0, distance / hovering.range);

			Vector3 damped = DampAndToY (thisShip.velocity, hovering.damping);

			Vector3 finalHover = (hover * normal) - damped;

			thisShip.AddForce (finalHover, ForceMode.VelocityChange);

			/*
			Vector3 rotCross = Vector3.Cross (transform.up, hovering.hit.normal);
			Vector3 rotDamp = DampAndToY (thisShip.angularVelocity, hovering.damping, true);
			Vector3 finalTorque = hover * (rotCross - rotDamp);

			//thisShip.AddTorque (finalTorque, ForceMode.VelocityChange);
			*/

			Debug.DrawLine (transform.position,
			                transform.position + finalHover,
			                Color.cyan);
			
			Debug.DrawLine (transform.position,
			                point,
			                Color.green);
		}
		else thisShip.useGravity = true;

	}

	float LerpLimit (float value, float limit) {
		value = Mathf.Lerp (value, 0, value / limit);
		return value;
	}
	
	float LerpCounter (float value, float limit) {
		value = Mathf.Lerp (0, value, value / limit);
		return value;
	}
	
	Vector3 LerpLimitAxes (Vector3 vector, Vector3 limit) {
		vector.x = LerpLimit (vector.x, limit.x);
		vector.y = LerpLimit (vector.y, limit.y);
		vector.z = LerpLimit (vector.z, limit.z);
		return vector;
	}
	
	Vector3 LerpCounterAxes (Vector3 vector, Vector3 limit) {
		vector.x = LerpCounter (vector.x, limit.x);
		vector.y = LerpCounter (vector.y, limit.y);
		vector.z = LerpCounter (vector.z, limit.z);
		return vector;
	}

	Vector3 DampAndToY (Vector3 input, float damp, bool invert = false) {
		Vector3 upVel = transform.InverseTransformVector (input);

		if (invert) upVel = Vector3.Scale (upVel, new Vector3 (damp, 0, damp));
		else upVel = Vector3.Scale (upVel, new Vector3 (0, damp, 0));

		upVel = transform.TransformVector (upVel);
		
		Debug.DrawLine (transform.position,
		                transform.position + upVel,
		                Color.green);
		return upVel;
	}
}

[System.Serializable]
public class Hover {
	public SphereCollider sphere;
	public float range = 5;
	public float height = 1;
	public float strength = 1;
	public float damping = .5f;
	[HideInInspector] public RaycastHit hit;

	public RaycastHit[] Cast () {

		Transform t = sphere.transform;

		return Physics.SphereCastAll (t.position, sphere.radius,
		                            -t.up, range);
	}
}

[System.Serializable]
public class Handling {
	public Vector2 steeringAngles;
	public Vector2 speed;
	public Vector2 grip;
	public float maxSpeed;
}