﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WTitle : MonoBehaviour
{
    public Button startButton;
    
    public void StartGame () {
        startButton.interactable = false;
        WGame.game.LoadScene ("thecar");
    }
}
