﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class WGame : MonoBehaviour {

	//public WInput input;


	Coroutine sceneLoad;

	public static WGame game;

	public InputActionAsset input;

	public bool startup;

	void Awake () {
		DontDestroyOnLoad (gameObject);
		WGame.game = this;
		if (startup) {
			startup = false;
			LoadScene ("title");
		}
	}

	/*
	void Update () {
		input.Update ();
	}*/

	public void LoadScene (string scene) {
		sceneLoad = StartCoroutine (DoLoadScene (scene));
	}

	public IEnumerator DoLoadScene (string scene) {
		yield return SceneManager.LoadSceneAsync ("loading", LoadSceneMode.Single);

		yield return SceneManager.LoadSceneAsync (scene, LoadSceneMode.Single);
	}

	public static int SumTrues (bool[] bools) {
		int sum = 0;
		foreach (bool i in bools) {
			if (i) sum++;
		}
		return sum;
	}
	
	public static int SumFalses (bool[] bools) {
		int sum = 0;
		foreach (bool i in bools) {
			if (!i) sum++;
		}
		return sum;
	}
	
	/*
	void GetInput () {
		input.moveAxes = new Vector3 (Input.GetAxisRaw ("Move X"),
		                        0,
		                        Input.GetAxisRaw ("Move Y"));
		
		input.camAxes = new Vector3 (Input.GetAxisRaw ("Cam Y"),
		                       Input.GetAxisRaw ("Cam X"),
		                       0);
		
		input.boostPress = Input.GetAxisRaw ("Boost");
		
		if (Input.GetButton ("Jump")) input.jump = true;
		else input.jump = false;
		
		if (Input.GetButton ("Attack")) input.attack = true;
		else input.attack = false;
		
		if (Input.GetButton ("Interact")) input.interact = true;
		else input.interact = false;
		
		if (Input.GetButton ("Reverse")) input.reverse = true;
		else input.reverse = false;
	}
	*/
}

/*
[System.Serializable]
public class WInput {
	public WStick moveAxes;
	public WStick camAxes;
	
	public WButton jump;
	public WButton attack;
	public WButton reverse;
	public WButton interact;
	public WAxis boost;

	public Touch touch;

	public void Update () {
		if (Input.touchCount > 0) touch = Input.GetTouch (0);
	}
}

[System.Serializable]
public class WButton {
	public string function;

	public bool IsDown () {
		return Input.GetButton (function);
	}
	
	public bool WasPressed () {
		return Input.GetButtonDown (function);
	}
	
	public bool WasReleased () {
		return Input.GetButtonUp (function);
	}
}

[System.Serializable]
public class WStick {
	public WAxis x;
	public WAxis y;

	public Vector3 MoveXY () {
		return new Vector3 (x.Strength(), y.Strength(), 0);
	}
	
	public Vector3 MoveXZ () {
		return new Vector3 (x.Strength(), 0, y.Strength());
	}
	
	public Vector3 TurnXY () {
		return new Vector3 (y.Strength(), x.Strength(), 0);
	}
	
	public Vector3 TurnXZ () {
		return new Vector3 (y.Strength(), 0, x.Strength());
	}
}

[System.Serializable]
public class WAxis {
	public string nameString;
	public float value;
	public float downThreshold;
	float prevStrength;
	
	public float Strength () {
		value = Input.GetAxisRaw (nameString);
		prevStrength = value;
		return value;
	}
	
	public float Strength (bool dontKeep) {
		value = Input.GetAxisRaw (nameString);
		if (!dontKeep) prevStrength = value;
		return value;
	}
	
	public bool IsDown () {
		if (Mathf.Abs (Strength()) >= downThreshold) {
			return true;
		}
		else return false;
	}
	
	public bool WasPressed () {
		if (IsDown() && prevStrength < downThreshold) return true;
		else return false;
	}
}

[System.Serializable]
public class WChargeable {
	public WButton button;
	
	public float basePower;
	public float chargedPower;
	public float chargeTime;
	
	public float charge = 0;
	public float finalPower;



	void Charge () {
		
		if (button.IsDown ()) {
			
			if (button.WasPressed ()) {
				charge = Time.fixedDeltaTime;
			}
			else {
				if (charge >= chargeTime) charge = Mathf.Clamp (charge,
				                                                          0,
				                                                          chargeTime);
				else charge += Time.fixedDeltaTime;
			}
		}
		else if (button.WasReleased ()) {
			charge = 0;
		}
		
		finalPower = basePower
					 + ((charge / chargeTime)
					 * (chargedPower - basePower));
	}
	
}*/