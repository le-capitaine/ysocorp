﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WCheckpoint : MonoBehaviour
{
    public WGameSystem system;

    void OnTriggerEnter (Collider other) {
        if (other.tag == "Player") system.Checkpoint ();
    }
}
