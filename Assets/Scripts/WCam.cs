﻿using UnityEngine;
using System.Collections;

public class WCam : MonoBehaviour {

	public Camera cam;
	
	public float speed;
	public float stiffness;

	public CamOrbit orbit;
	[HideInInspector] public CamMovement movement;

	
	void Awake () {
		if (orbit.target) transform.parent = null;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (orbit.target) Orbit ();
		Move ();
	}

	void Move () {
		transform.position = Vector3.Lerp (transform.position,
		                                   movement.targetPos, stiffness);
		transform.rotation = Quaternion.Lerp (transform.rotation,
		                                      movement.targetRot, stiffness);
	}

	void Orbit () {
		//orbit.degrees += (WGame.game.input.camAxes.TurnXY() * speed);
		orbit.degrees.x = Mathf.Clamp (orbit.degrees.x,
		                               orbit.clamp.x,
		                               orbit.clamp.y);
		
		movement.targetRot = Quaternion.Euler (orbit.degrees);


		movement.flatRot = Quaternion.Euler (0,
		                            transform.eulerAngles.y,
		                            0);
		
		
		Vector3 flatOffset = movement.flatRot * orbit.offset;

		Vector3 widener = Vector3.Lerp (Vector3.zero,
		                                orbit.widening,
		                                orbit.degrees.x / orbit.clamp.y);
		
		/*
		Vector3 camRay = (camTargetPos - transform.position);
		RaycastHit[] camCols = Physics.SphereCastAll (transform.position,
		                                              camRadius,
		                                              camRay,
		                                              camRay.magnitude);
		int total = camCols.Length;
		Vector3 colNormal = Vector3.zero;
		
		if (total > 0) {
			for (int i = 0; i < total; i++) {
				colNormal += camCols[i].point
					+	(camCols[i].normal * camRadius);
			}
			colNormal /= total;
		}
		*/
		
		movement.targetPos = orbit.target.transform.position + flatOffset
			+ transform.TransformDirection(orbit.offset + widener);
	}
}

[System.Serializable]
public class CamOrbit {
	public GameObject target;
	public Vector3 degrees;
	public Vector2 clamp;
	public Vector3 offset;
	public Vector3 widening;
}

[System.Serializable]
public class CamMovement {
	public Vector3 targetPos;
	public Quaternion targetRot;
	public Quaternion flatRot;
}
