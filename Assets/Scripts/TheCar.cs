﻿using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections;

public class TheCar : MonoBehaviour {

	public WGameSystem system;
	public Rigidbody thisRigidbody;

	public bool getsInput;

	public GameObject[] wheelMeshes;
	public WheelCollider[] wheelCols;
	public ParticleSystem[] wheelDust;

	public AudioSource engine;
	public AudioSource[] wheelDrift;



	public float steeringTightness;
	public float speed;
	public float boostPower;
	public bool[] drivetrain;
	public bool[] brakes;
	public float[] steering;

	public Transform reverseTarget;
	public float reverseDistance;

	//public WChargeable jump;
	//public WChargeable ram;
	//public float airControl;
	//public float airControlCap;
	public Vector3 centerOfMass;

	public Transform arrow;


	public float camForward;
	public float camPitch;
	public float camWiden;
	
	//WInput input;


	Vector2 dir;

	Vector3 targetDir;

	float throttle;
	float reverseTorque;

	//bool grounded;

	[HideInInspector] public float zVel;


	void Start () {
		thisRigidbody.centerOfMass = centerOfMass;
		InputInit ();
		//input = WGame.game.input;
		//jump.button.function = game.input.jump.function;
		//ram.button.function = game.input.attack.function;
	}

	void InputInit () {
		WGame.game.input ["thecar/Move"].performed += context => {
			Vector2 axes = context.ReadValue<Vector2> ();
			dir = axes;
		};

		WGame.game.input ["thecar/Move"].canceled += context => {
			dir = Vector2.zero;
		};

		WGame.game.input ["thecar/Move"].Enable ();
	}
	
	void FixedUpdate () {
		Drive ();
		WheelUpdate ();
	}

	/*
	void OnCollisionStay (Collision other) {
		if (input.jump.WasReleased ()) {

			Vector3 meanNormal = Vector3.zero;
			foreach (ContactPoint contact in other.contacts) meanNormal += contact.normal;
			meanNormal /= other.contacts.Length;

			thisRigidbody.AddForce (meanNormal * jump.finalPower,
					                ForceMode.VelocityChange);
		}
	}*/

	void WheelUpdate () {
		for (int i = 0; i < 4; i++) {
			Vector3 pos;
			Quaternion rot;
			wheelCols[i].GetWorldPose(out pos, out rot);
			wheelMeshes[i].transform.position = pos;
			wheelMeshes[i].transform.rotation = rot;
		}
	}

	/*
	void AxisUpdate () {
		throttle = 0f;
		//throttle += input.moveAxes.MoveXZ().magnitude;
		if (Input.touchCount > 0) throttle += 1f; //throttle += Mathf.Clamp01 (Input.GetTouch (0).radius);
		else if (Input.GetMouseButton (0)) throttle += 1f;

		throttle = Mathf.Clamp01 (throttle);
	}*/

	void Drive () {
		//AxisUpdate ();

		throttle = dir.magnitude * 2f;

		/*
		RaycastHit groundHit;
		Vector3 jumpNormal = Vector3.zero;

		if (Physics.Raycast (transform.position + (transform.up * .1f),
		                     -transform.up,
		                     out groundHit,
		                     .3f)) {
			grounded = true;
			jumpNormal = groundHit.normal;
		}
		else grounded = false;
		*/
		
		

		/*
		Vector3 targetDir = system.playerCam.movement.flatRot
			*	input.moveAxes.MoveXZ();*/

		Vector3 targetDir = system.playerCam.movement.flatRot * new Vector3 (dir.x, 0f, dir.y);


		//Vector3 vtouch;
		
		/*
		if (Input.touchCount > 0) {
			Touch touch = input.touch;
			vtouch = new Vector3 (touch.position.x, touch.position.y, 200f);
		}

		else*/

		/*
		vtouch = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 200f);
		Ray touchDir = system.playerCam.cam.ScreenPointToRay (vtouch);

		RaycastHit touchHit;
		Vector3 touchTarget;

		if (Physics.Raycast (touchDir, out touchHit)) touchTarget = touchHit.point;
		else touchTarget = touchDir.GetPoint (200f);


		targetDir += (touchTarget - transform.position).normalized;

		targetDir = Vector3.ClampMagnitude (targetDir, 1);*/


		Vector3 flatDir = new Vector3 (transform.forward.x,
		                               0,
		                               transform.forward.z).normalized;
		float sign = Vector3.Cross (flatDir, targetDir).y;
		
		/*
		if (!grounded) {
			Vector3 influence = targetDir * airControl;
			Vector3 flatVel = new Vector3 (thisRigidbody.velocity.x,
			                               0,
			                               thisRigidbody.velocity.z);
			Vector3 capVector = new Vector3 (Mathf.Lerp (0, influence.x, Mathf.Abs (flatVel.x) / airControlCap),
			                                 0,
			                                 Mathf.Lerp (0, influence.z, Mathf.Abs (flatVel.z) / airControlCap));
			thisRigidbody.AddForce (influence - capVector,
			                        ForceMode.VelocityChange);
		}
		else {
			if (jump.button.IsDown()) {
				thisRigidbody.AddForce (-jumpNormal * jump.finalPower,
				                        ForceMode.Acceleration);
			}
			else if (jump.button.WasReleased()) {
				thisRigidbody.AddForce (jumpNormal * jump.finalPower,
			                        	ForceMode.Acceleration);
			}

			if (ram.button.WasReleased()) {
				thisRigidbody.AddForce (transform.forward * ram.finalPower,
				                        ForceMode.Acceleration);
			}
		}*/

		float torque = Mathf.Lerp (0, speed, throttle)
			//+ Mathf.Lerp (0, boostPower, input.boost.Strength())
			* Mathf.Lerp (1,
			              0,
			              Vector3.Angle (flatDir,
			               targetDir) / 180);

		float brake = Mathf.Lerp (speed, 0, throttle / .25f);

		int torqueDiv = WGame.SumTrues (drivetrain);
		int brakeDiv = WGame.SumTrues (brakes);

		torque /= torqueDiv;
		brake /= brakeDiv;

		float steer = 0f;
		
		if (getsInput) {
			//if ((touchTarget - reverseTarget.position).sqrMagnitude < reverseDistance * reverseDistance) {
			if (Vector3.Dot (flatDir, dir) < -.8f) {
				//arrow.rotation = Quaternion.LookRotation (Vector3.ProjectOnPlane (-transform.forward, Vector3.up), Vector3.up);
				torque = -torque;
			}
			else {
				//arrow.rotation = Quaternion.LookRotation (Vector3.ProjectOnPlane (targetDir, Vector3.up), Vector3.up);
				steer = Vector3.Angle (flatDir, targetDir) * sign;
			}
		}
		else {
			torque = 0f;
			brake = 1f;
		}

		engine.pitch = Mathf.Lerp (engine.pitch, Mathf.Clamp01 (torque + .25f) * 2f, .025f);

		
		for (int i = 0; i < steering.Length; i++) {
			if (steering[i] != 0) {
				steer = Mathf.Clamp (steer, -steering[i], steering[i]);

				wheelCols[i].steerAngle = Mathf.Lerp (wheelCols[i].steerAngle,
													steer,
													steeringTightness);
			}
		}

		for (int i = 0; i < wheelCols.Length; i++) {
			WheelCollider wheel = wheelCols[i];
			if (drivetrain[i]) wheel.motorTorque = torque;
			//if (!input.reverse.IsDown() && brakes[i]) wheelCols[i].brakeTorque = brake;
		}

		for (int i = 0; i < wheelCols.Length; i++) {
			WheelHit wheelHit;
			wheelCols [i].GetGroundHit (out wheelHit);
			ParticleSystem dust = wheelDust [i];
			ParticleSystem.EmissionModule emission = dust.emission;

			float slip = Mathf.Clamp01 (Mathf.Abs (wheelHit.forwardSlip) + Mathf.Abs (wheelHit.sidewaysSlip));

			emission.rateOverDistanceMultiplier = slip;
			
			AudioSource drift = wheelDrift [i];

			drift.volume = Mathf.Lerp (drift.volume, slip * .5f, .2f);
			drift.pitch = Mathf.Lerp (drift.pitch, slip + .25f, .2f);
		}

		/*
		Debug.DrawLine (transform.position,
		                transform.position + (targetDir * 2),
		                Color.red);
		Debug.DrawLine (transform.position,
		                transform.position + (flatDir * 2),
		                Color.cyan);
		*/
	}
}
